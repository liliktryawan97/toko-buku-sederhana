-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2021 at 07:59 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `book_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_book`
--

CREATE TABLE `tb_book` (
  `id_bk` int(11) NOT NULL,
  `judul_bk` varchar(60) NOT NULL,
  `keterangan` varchar(500) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(4) NOT NULL,
  `gambar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_book`
--

INSERT INTO `tb_book` (`id_bk`, `judul_bk`, `keterangan`, `kategori`, `harga`, `stok`, `gambar`) VALUES
(1, 'Basic Programming Python', 'Dasar-dasar python', 'Programmer', 160000, 26, 'bukuddp.png'),
(3, 'Struktur Data dan Algoritma Python', 'Struktur Data Python', 'Programmer', 185000, 14, 'bukusda.png'),
(4, 'Pemrograman Berbasis Objek Java', 'PBO Dengan Java', 'Programmer', 205000, 27, 'java.png'),
(5, 'Kesehatan Mental Dalam Perspektif', 'Kesehatan mental', 'Kesehatan', 75000, 18, 'kesehatan.png'),
(15, 'Biologi untuk pengetahuan umum', 'Buku biologi umum', 'Pendidikan', 85000, 76, 'biologi.png'),
(16, 'Saintek masyarakat lingkungan', 'Sains untuk pemula, Sains, Teknologi, Lingkungan dan Masyarakat', 'Sains dan Teknologi', 84000, 64, 'saintek.png'),
(17, 'Novel terlaris Mariposa', 'Novel ringan Mariposa', 'Novel', 74000, 91, 'novel.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pesanan`
--

CREATE TABLE `tb_pesanan` (
  `id` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `judul_bk` varchar(40) NOT NULL,
  `jumlah` int(3) NOT NULL,
  `harga` int(10) NOT NULL,
  `pilihan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pesanan`
--

INSERT INTO `tb_pesanan` (`id`, `id_transaksi`, `id_buku`, `judul_bk`, `jumlah`, `harga`, `pilihan`) VALUES
(1, 2, 1, 'Basic Programming Python', 1, 160000, ''),
(2, 2, 3, 'Struktur Data dan Algoritma Python', 1, 185000, ''),
(3, 2, 4, 'Pemrograman Berbasis Objek Java', 1, 205000, ''),
(4, 3, 4, 'Pemrograman Berbasis Objek Java', 2, 205000, ''),
(5, 3, 1, 'Basic Programming Python', 2, 160000, ''),
(6, 3, 5, 'Kesehatan Mental Dalam Perspektif', 1, 75000, ''),
(7, 3, 3, 'Struktur Data dan Algoritma Python', 1, 185000, ''),
(9, 5, 3, 'Struktur Data dan Algoritma Python', 1, 185000, ''),
(10, 6, 16, 'Saintek masyarakat lingkungan', 1, 84000, '');

--
-- Triggers `tb_pesanan`
--
DELIMITER $$
CREATE TRIGGER `pesanan_buku` AFTER INSERT ON `tb_pesanan` FOR EACH ROW BEGIN
	UPDATE tb_book SET stok = stok-new.jumlah
    WHERE id_bk=new.id_buku;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi`
--

CREATE TABLE `tb_transaksi` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `tgl_pesan` datetime NOT NULL,
  `batas_pembayaran` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_transaksi`
--

INSERT INTO `tb_transaksi` (`id`, `nama`, `alamat`, `no_telp`, `tgl_pesan`, `batas_pembayaran`) VALUES
(1, 'Lilik Triyawan', 'Jakarta', '02198592165126', '2021-06-28 01:07:36', '2021-06-29 01:07:36'),
(2, 'Lilik Triyawan', 'Jakarta', '02198592165126', '2021-06-28 01:10:15', '2021-06-29 01:10:15'),
(3, 'Budi', 'Depok', '0218484815', '2021-06-28 21:35:07', '2021-06-29 21:35:07'),
(4, 'Anna', 'Bogor', '0825481648', '2021-06-30 00:16:15', '2021-07-01 00:16:15'),
(5, 'Anna', 'Bogor', '0825481648', '2021-06-30 00:19:08', '2021-07-01 00:19:08'),
(6, 'Hani', 'Jakarta', '08255148', '2021-06-30 00:20:19', '2021-07-01 00:20:19');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `role_id` tinyint(1) NOT NULL,
  `alamat` varchar(40) NOT NULL,
  `no_telp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`, `role_id`, `alamat`, `no_telp`) VALUES
(1, 'Lilik', 'Liliktriyawan', '123', 1, 'Jakarta', '02147852'),
(2, 'Budi', 'budi123', '1234', 2, 'Jakarta', '02147852785');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_book`
--
ALTER TABLE `tb_book`
  ADD PRIMARY KEY (`id_bk`);

--
-- Indexes for table `tb_pesanan`
--
ALTER TABLE `tb_pesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_book`
--
ALTER TABLE `tb_book`
  MODIFY `id_bk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tb_pesanan`
--
ALTER TABLE `tb_pesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

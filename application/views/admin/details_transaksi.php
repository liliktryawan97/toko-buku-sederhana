<div class="container-fluid">
    <h4>Detail Transaksi dengan nomor Pesanan <?php echo $transaksi->id ?></h4>
    <div class="row text-left mt-2">
        <table class="table table-bordered table-hover table-striped">
            <tr align='center'>
                <th>ID Buku</th>
                <th>Judul Buku</th>
                <th>Jumlah Pesanan</th>
                <th>Harga Buku</th>
                <th>Total</th>
            </tr>

            <?php 
            $total = 0;
            foreach($pesanan as $pesan) :
                $subtotal = $pesan->jumlah * $pesan->harga;
                $total += $subtotal;
            ?>

            <tr>
                <td align="center"><?php echo $pesan->id_buku ?></td>
                <td><?php echo $pesan->judul_bk ?></td>
                <td align="center"><?php echo $pesan->jumlah ?></td>
                <td class="text-right">Rp. <?php echo number_format($pesan->harga, 0, ',', '.') ?></td>
                <td class="text-right">Rp. <?php echo number_format($subtotal, 0, ',', '.') ?></td>
            </tr>
            <?php endforeach; ?>

            <tr>
                <td colspan="4" align="left">Jumlah Total</td>
                <td align="right">Rp. <?php echo number_format($total, 0, ',', '.') ?></td>
            </tr>
        </table>
        <a href="<?php echo base_url('admin/transaksi/index') ?>"><div class="btn btn-sm btn-primary">Kembali</div></a>
    </div>
</div>
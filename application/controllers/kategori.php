<?php

class Kategori extends CI_Controller{
    public function pemrograman() {
        $data['pemrograman']=$this->m_kategori->data_buku_pemrograman()-> result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pemrograman', $data);
        $this->load->view('templates/footer');
    }

    public function sains_dan_teknologi() {
        $data['saintek']=$this->m_kategori->data_buku_saintek()-> result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('saintek', $data);
        $this->load->view('templates/footer');
    }

    public function novel() {
        $data['novel']=$this->m_kategori->data_buku_novel()-> result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('novel', $data);
        $this->load->view('templates/footer');
    }

    public function kesehatan() {
        $data['kesehatan']=$this->m_kategori->data_buku_kesehatan()-> result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('kesehatan', $data);
        $this->load->view('templates/footer');
    }

    public function pendidikan() {
        $data['pendidikan']=$this->m_kategori->data_buku_pendidikan()-> result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pendidikan', $data);
        $this->load->view('templates/footer');
    }
}
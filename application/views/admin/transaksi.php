<div class="container-fluid">
    <h5>Transaksi Buku</h5>

    <table class="table table-bordered table-hover table-strip">
        <tr class="text-center">
            <th>Id Transaksi</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>No. Telepon</th>
            <th>Tanggal Pesan</th>
            <th>Batas Pembayaran</th>
            <th>Detail Transaksi</th>
        </tr>

        <?php foreach($transaksi as $trans): ?>
        <tr>
            <td class="text-center"><?php echo $trans->id ?></td>
            <td><?php echo $trans->nama ?></td>
            <td><?php echo $trans->alamat ?></td>
            <td><?php echo $trans->no_telp ?></td>
            <td><?php echo $trans->tgl_pesan ?></td>
            <td><?php echo $trans->batas_pembayaran ?></td>
            <td class="text-center"><?php echo anchor('admin/transaksi/details/'.$trans->id, '<div class="btn btn-sm btn-primary">Detail</div>') ?></td>
        </tr>

        <?php endforeach; ?>
    </table>
</div>
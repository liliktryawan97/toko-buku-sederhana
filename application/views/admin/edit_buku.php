<div class="container-fluid">
    <h3><i class="fas fa-edit"></i> Edit Data Buku</h3>

    <?php foreach ($buku as $bk): ?>

        <form method="post" action="<?php echo base_url().'admin/data_buku/update' ?>">

        <div class="form-group">
            <label for="">Judul Buku</label>
            <input type="hidden", name="id_bk", class="form-control", value="<?php echo $bk->id_bk ?>">
            <input type="text", name="judul_bk", class="form-control", value="<?php echo $bk->judul_bk ?>">
        </div>

        <div class="form-group">
            <label for="">Keterangan</label>
            <input type="text", name="keterangan", class="form-control", value="<?php echo $bk->keterangan ?>">
        </div>

        <div class="form-group">
            <label for="">Kategori</label>
            <input type="text", name="kategori", class="form-control", value="<?php echo $bk->kategori ?>">
        </div>

        <div class="form-group">
            <label for="">Harga</label>
            <input type="text", name="harga", class="form-control", value="<?php echo $bk->harga ?>">
        </div>

        <div class="form-group">
            <label for="">Quantitas</label>
            <input type="text", name="stok", class="form-control", value="<?php echo $bk->stok ?>">
        </div>

        <button type="sunmit" class="btn btn-primary" mt-3> Save</button>

        </form>

    <?php endforeach; ?>
</div>
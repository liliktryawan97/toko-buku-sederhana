<div class="container-fluid">
    <h3>Keranjang Belanja</h3>

    <table class="table table-bordered table-striped table-hover">
        <tr class="text-center">
            <th>No</th>
            <th>Judul Buku</th>
            <th>Jumlah</th>
            <th>Harga</th>
            <th>Total</th>
        </tr>

        <?php
        $no = 1;
        foreach ($this->cart->contents() as $buku) : ?>

            <tr>
                <td class="text-center"><?php echo $no++ ?></td>
                <td><?php echo $buku['name'] ?></td>
                <td class="text-center"><?php echo $buku['qty'] ?></td>
                <td class="text-right">Rp. <?php echo number_format($buku['price'], 0, ',','.') ?></td>
                <td class="text-right">Rp. <?php echo number_format($buku['subtotal'], 0, ',','.') ?></td>
            </tr>

        <?php endforeach; ?>

        <tr>
            <td colspan="4">Jumlah Total</td>
            <td class="text-right">Rp. <?php echo number_format($this->cart->total(), 0, ',','.') ?></td>
        </tr>
    </table>

    <div align = "right">
        <a href="<?php echo base_url('welcome') ?>"><div class="btn btn-sm btn-primary">Tambahkan</div></a>
        <a href="<?php echo base_url('dashboard/order') ?>"><div class="btn btn-sm btn-success">Pesan</div></a>
        <a href="<?php echo base_url('dashboard/hapus_troli') ?>"><div class="btn btn-sm btn-danger">Hapus</div></a>
    </div>
</div>
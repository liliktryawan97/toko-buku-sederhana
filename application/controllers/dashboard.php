<?php

class Dashboard extends CI_Controller{

    public function __construct(){
        parent::__construct();

        if($this->session->userdata('role_id') != '2'){
            $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show"      role="alert">
            <strong>Maaf!</strong> Kamu Belum Log in
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>');
        redirect('auth/login');
        }
    }
    
    public function troli($id) {
        $buku = $this->m_buku->find($id);
        $data = array(
            'id'      => $buku->id_bk,
            'qty'     => 1,
            'price'   => $buku->harga,
            'name'    => $buku->judul_bk,
    );
    
    $this->cart->insert($data);
    redirect('welcome');
    }

    public function detail_troli() {
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('troli');
        $this->load->view('templates/footer');
    }

    public function hapus_troli() {
        $this->cart->destroy();
        redirect('welcome');
    }

    public function order() {
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('orders');
        $this->load->view('templates/footer');
    }

    public function proses_order() {
        $is_processed = $this->m_transaksi->index();
        if($is_processed) {
            $this->cart->destroy();
            $this->load->view('templates/header');
            $this->load->view('templates/sidebar');
            $this->load->view('proses_order');
            $this->load->view('templates/footer');
        } else {
            echo "Pesanan Kamu tidak bisa diproses";
        }
    }

    public function detail($id_buku) {
        $data['buku'] = $this->m_buku->description($id_buku);
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('desc_buku', $data);
        $this->load->view('templates/footer');
    }
}

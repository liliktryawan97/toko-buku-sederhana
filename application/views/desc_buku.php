<div class="container-fluid">
    <div class="card">
        <h5 class="card-header">Deskripsi Buku</h5>
        <div class="card-body">
            <?php foreach($buku as $bk) : ?>   
            <div class="row">

                <div class="col-md-4">
                    <img src="<?php echo base_url().'/uploads/'.$bk->gambar ?>" class="card-img-top">
                </div>
                <div class="col-md-8">
                    <table class="table">
                        <tr>
                            <td>Judul Buku </td>
                            <td><strong><?php echo $bk->judul_bk ?></strong></td>
                        </tr>

                        <tr>
                            <td>Deskripsi </td>
                            <td><strong><?php echo $bk->keterangan ?></strong></td>
                        </tr>

                        <tr>
                            <td>Kategori </td>
                            <td><strong><?php echo $bk->kategori ?></strong></td>
                        </tr>

                        <tr>
                            <td>Jumlah Buku </td>
                            <td><strong><?php echo $bk->stok ?></strong></td>
                        </tr>

                        <tr>
                            <td>Harga </td>
                            <td><strong><div class="btn btn-sm btn-primary">Rp. <?php echo number_format($bk->harga, 0, ',','.') ?></div></strong></td>
                        </tr>
                    </table>
                    <?php echo anchor('dashboard/troli/'. $bk->id_bk, '<div class="btn btn-sm btn-success" align="right">Tambah ke Troli</div>') ?>
                    <?php echo anchor('dashboard/index/', '<div class="btn btn-sm btn-danger">Kembali</div>') ?>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
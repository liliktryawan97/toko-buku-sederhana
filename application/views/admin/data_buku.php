<div class="container-fluid">
    <h3><i class="fas fa-database"></i> Data Buku</h3>
    <button class="btn btn-sm btn-primary mb-3" data-toggle="modal" data-target="#tambah_buku"><i class = "fas fa-plus fa-sm"></i> Tambahkan Buku</button>

    <table class="table table-bordered">
        <tr class="text-center">
            <th>No.</th>
            <th>Judul Buku</th>
            <th>Keterangan</th>
            <th>Kategori</th>
            <th>Harga</th>
            <th>Quantitas</th>
            <th colspan="3">Dial</th>
        </tr>

        <?php
        $no = 1;
        foreach($buku as $bk) :?>
        <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $bk->judul_bk ?></td>
            <td><?php echo $bk->keterangan ?></td>
            <td><?php echo $bk->kategori ?></td>
            <td><?php echo $bk->harga ?></td>
            <td><?php echo $bk->stok ?></td>
            <td><div class="btn btn-success btn-sm"><i class ="fas fa-search-plus"></i></div></td>
            <td><?php echo anchor('admin/data_buku/edit/'. $bk->id_bk, '<div class="btn btn-primary btn-sm"><i class ="fas fa-edit"></i></div>') ?></td>
            <td><?php echo anchor('admin/data_buku/hapus/'. $bk->id_bk, '<div class="btn btn-danger btn-sm"><i class ="fas fa-trash"></i></div>') ?></td>

        </tr>

            <?php endforeach; ?>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="tambah_buku" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Tambahkan Buku</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url(). 'admin/data_buku/tambah_buku' ?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="">Judul Buku</label>
                <input type="text" name="judul_bk" class="form-control">
            </div>

            <div class="form-group">
                <label for="">Keterangan</label>
                <input type="text" name="keterangan" class="form-control">
            </div>

            <div class="form-group">
                <label for="">Kategori</label>
                <select name="kategori" id="" class="form-control">
                <option value="">Programmer</option>
                <option value="">Sains dan Teknologi</option>
                <option value="">Novel</option>
                <option value="">Kesehatan</option>
                <option value="">Pendidikan</option>
                </select>

            </div>

            <div class="form-group">
                <label for="">Harga</label>
                <input type="text" name="harga" class="form-control">
            </div>

            <div class="form-group">
                <label for="">Quantitas</label>
                <input type="text" name="stok" class="form-control">
            </div>

            <div class="form-group">
                <label for="">Gambar Produk</label><br>
                <input type="file" name="gambar" class="form-control">
            </div>

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Kembali</button>
        <button type="submit" class="btn btn-primary">Tambahkan</button>
      </div>

      </form>
      
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="btn btn-sm btn-primary">
                <?php 
                $total = 0;
                if ($troli = $this->cart->contents())
                {
                    foreach ($troli as $buku) {
                        $total = $total + $buku['subtotal'];
                    }

                    echo "Total belanja kamu: Rp. ".number_format($total,0,',','.');
                ?>
            </div><br><br>
            <h3>Masukan Alamat dan Pembayaran</h3>

            <form method="post" action="<?php echo base_url() ?> dashboard/proses_order">
                <div class="form-group">
                    <label>Nama Lengkap</label>
                    <input type="text" name="nama" placeholder="Nama Lengkap" class="form-control">
                </div>

                <div class="form-group">
                    <label>Alamat Lengkap</label>
                    <input type="text" name="alamat" placeholder="Alamat Lengkap" class="form-control">
                </div>

                <div class="form-group">
                    <label>No. Telp</label>
                    <input type="text" name="no_telp" placeholder="No. Telepon" class="form-control">
                </div>

                <div class="form-group">
                    <label>Metode Pengiriman</label>
                    <select name="" id="" class="form-control">
                        <option value="">JNE</option>
                        <option value="">JNT</option>
                        <option value="">SiCepat</option>
                        <option value="">Ninja Express</option>
                        <option value="">TIKI</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Metode Pembayaran</label>
                    <select name="" id="" class="form-control">
                        <option value="">BCA</option>
                        <option value="">BNI</option>
                        <option value="">BRI</option>
                        <option value="">Mandiri</option>
                        <option value="">CIMB</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-sm btn-success">Pesan</button>
            </form>

            <?php 
        } else {
            echo "Kamu belum memiliki pesanan";
        }
             ?>
        </div>

        <div class="col-md-2"></div>
    </div>
</div>
<?php
class m_transaksi extends CI_Model{
    public function index() {
        date_default_timezone_set('Asia/Jakarta');
        $nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $no_telp = $this->input->post('no_telp');

        $transaksi = array (
            'nama'              => $nama,
            'alamat'            => $alamat,
            'no_telp'           => $no_telp,
            'tgl_pesan'         => date('Y-m-d H:i:s'),
            'batas_pembayaran'  => date('Y-m-d H:i:s', mktime(date('H'), date('i'), date('s'), date('m'), date('d') + 1, date('Y'))),
        );
        $this->db->insert('tb_transaksi', $transaksi);
        $id_transaksi = $this->db->insert_id();

        foreach ($this->cart->contents() as $buku) {
            $data = array (
                'id_transaksi'  => $id_transaksi,
                'id_buku'       => $buku['id'],
                'judul_bk'      => $buku['name'],
                'jumlah'        => $buku['qty'],
                'harga'         => $buku['price'],
            );
            $this->db->insert('tb_pesanan', $data);
        }
        return TRUE;
    }

    public function tampilkan_data() {
        $result = $this->db->get('tb_transaksi');
        if($result->num_rows() > 0) {
            return $result->result();
        }else{
            return false;
        }
    }

    public function ambil_id_transaksi($id_transaksi) {
        $result = $this->db->where('id', $id_transaksi)->limit(1)->get('tb_transaksi');
        if($result->num_rows() > 0) {
            return $result->row();
        }else{
            return false;
        }
    }

    public function ambil_id_pesanan($id_transaksi) {
        $result = $this->db->where('id_transaksi', $id_transaksi)->get('tb_pesanan');
        if($result->num_rows() > 0) {
             return $result->result();
        }else{
            return false;
        }
    }
}
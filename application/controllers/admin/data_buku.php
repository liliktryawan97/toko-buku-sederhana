<?php

class Data_buku extends CI_Controller {

    public function __construct(){
        parent::__construct();

        if($this->session->userdata('role_id') != '1'){
            $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show"      role="alert">
            <strong>Maaf!</strong> Kamu Belum Log in
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>');
        redirect('auth/login');
        }
    }
    public function index() {
        $data['buku'] = $this->m_buku->tampil_data()->result();
        $this->load->view('temp_admin/header');
        $this->load->view('temp_admin/sidebar');
        $this->load->view('admin/data_buku', $data);
        $this->load->view('temp_admin/footer');
    }

    public function tambah_buku() {
        $judul_bk   = $this->input->post('judul_bk');
        $keterangan = $this->input->post('keterangan');
        $kategori   = $this->input->post('kategori');
        $harga      = $this->input->post('harga');
        $stok       = $this->input->post('stok');
        $gambar     = $_FILES['gambar']['name'];
        if ($gambar = ''){}else{
            $config ['upload_path'] = './uploads';
            $config ['allowed_types'] = 'jpg|jpeg|png|gif';

            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('gambar')) {
                echo "Gagal Menambahkan Gambar";
            }else{
                $gambar=$this->upload->data('file_name'); 
            }
        }
        $data = array(
            'judul_bk'  => $judul_bk,
            'keterangan' => $keterangan,
            'kategori' => $kategori,
            'harga' => $harga,
            'stok' => $stok,
            'gambar' => $gambar
        );
        $this->m_buku->tambah_buku('tb_book', $data);
        redirect('admin/data_buku/index');
    }

    public function edit($id) {
        $where = array ('id_bk'=>$id);
        $data['buku']= $this->m_buku->edit_buku($where, 'tb_book')->result();
        $this->load->view('temp_admin/header');
        $this->load->view('temp_admin/sidebar');
        $this->load->view('admin/edit_buku', $data);
        $this->load->view('temp_admin/footer');
    }

    public function update() {
        $id_bk = $this->input->post('id_bk');
        $judul_bk = $this->input->post('judul_bk');
        $keterangan = $this->input->post('keterangan');
        $kategori = $this->input->post('kategori');
        $harga = $this->input->post('harga');
        $stok = $this->input->post('stok');

        $data = array(
            'judul_bk' => $judul_bk,
            'keterangan' => $keterangan,
            'kategori' => $kategori,
            'harga' => $harga,
            'stok' => $stok
        );
        $where = array(
            'id_bk' => $id_bk
        );

        $this->m_buku->update_data_buku($where, $data, 'tb_book');
        redirect('admin/data_buku/index');
    }

    public function hapus($id) {
        $where = array(
            'id_bk' => $id
        );
        $this->m_buku->hapus_data_buku($where, 'tb_book');
        redirect('admin/data_buku/index');
    }
}
<?php

class M_kategori extends CI_Model{
    public function data_buku_pemrograman() {
        return $this->db->get_where("tb_book", array('kategori'=>'programmer'));
    }

    public function data_buku_saintek() {
        return $this->db->get_where("tb_book", array('kategori'=>'sains dan teknologi'));
    }

    public function data_buku_novel() {
        return $this->db->get_where("tb_book", array('kategori'=>'novel'));
    }

    public function data_buku_kesehatan() {
        return $this->db->get_where("tb_book", array('kategori'=>'kesehatan'));
    }

    public function data_buku_pendidikan() {
        return $this->db->get_where("tb_book", array('kategori'=>'pendidikan'));
    }
}
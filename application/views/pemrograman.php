<div class="container-fluid">

    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
            <!-- <li data-target="#carouselExampleCaptions" data-slide-to="2"></li> -->
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="<?php echo base_url('assets/img/slider2.png') ?>" class="d-block w-100" alt="...">
            <!-- <div class="carousel-caption d-none d-md-block">
                <h5>First slide label</h5>
                <p>Some representative placeholder content for the first slide.</p>
            </div> -->
            </div>
            <div class="carousel-item">
            <img src="<?php echo base_url('assets/img/slider1.png') ?>" class="d-block w-100" alt="...">
            <!-- <div class="carousel-caption d-none d-md-block">
                <!-- <h5>Second slide label</h5>
                <p>Some representative placeholder content for the second slide.</p><br> -->
            </div> -->
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <div class="row text-left mt-4">

        <?php foreach ($pemrograman as $bk) : ?>

            <div class="card ml-3" style="width: 14rem;">
                <img src="<?php echo base_url('uploads/'. $bk->gambar) ?>" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title mb-1"><?php echo $bk->judul_bk ?></h5>
                    <!-- <small><?php echo $bk->keterangan ?></small><br> -->
                    <div class="text-center">
                    <span class="badge badge-danger mb-2">Rp. <?php echo number_format($bk->harga, 0, ',','.') ?></span><br>
                    <?php echo anchor('dashboard/detail/'. $bk->id_bk, '<div class="btn btn-sm btn-success">Deskripsi</div>') ?>
                    <!-- <a href="#" class="btn btn-sm btn-info">Detail</a> -->
                    <?php echo anchor('dashboard/troli/'. $bk->id_bk, '<div class="btn btn-sm btn-primary">Pesan</div>') ?>
                    </div>
                    <!-- <a href="#" class="btn btn-sm btn-primary">Order</a> -->
                </div>
            </div>

        <?php endforeach; ?>
    </div>
</div>
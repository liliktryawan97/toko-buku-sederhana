<?php

class M_buku extends CI_Model{
    public function tampil_data() {
        return $this->db->get('tb_book');
    }

    public function tambah_buku($table, $data) {
        $this->db->insert($table, $data);
    }

    public function edit_buku($where, $table) {
        return $this->db->get_where($table, $where);
    }

    public function update_data_buku($where, $data, $table) {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function hapus_data_buku($where, $table){
        $this->db->where($where);
        $this->db->delete($table);
    }
    public function find($id) {
        $result = $this->db->where('id_bk', $id)
                           ->limit(1)
                           ->get('tb_book');
        if($result->num_rows() > 0){
            return $result->row();
        }else{
            return array();
        }
    }
    public function description($id_buku) {
        $result = $this->db->where('id_bk', $id_buku)->get('tb_book');
        if($result->num_rows() > 0){
            return $result->result();
        }else{
            return false;
        }
    }
}

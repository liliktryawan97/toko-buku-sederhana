<?php 

class Transaksi extends CI_Controller{

    public function __construct(){
        parent::__construct();

        if($this->session->userdata('role_id') != '1'){
            $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show"      role="alert">
            <strong>Maaf!</strong> Kamu Belum Log in
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>');
        redirect('auth/login');
        }
    }
    public function index() {
        $data['transaksi'] = $this->m_transaksi->tampilkan_data();
        $this->load->view('temp_admin/header');
        $this->load->view('temp_admin/sidebar');
        $this->load->view('admin/transaksi',$data);
        $this->load->view('temp_admin/footer');
    }

    public function details($id_transaksi) {
        $data['transaksi'] = $this->m_transaksi->ambil_id_transaksi($id_transaksi);
        $data['pesanan'] = $this->m_transaksi->ambil_id_pesanan($id_transaksi);
        $this->load->view('temp_admin/header');
        $this->load->view('temp_admin/sidebar');
        $this->load->view('admin/details_transaksi',$data);
        $this->load->view('temp_admin/footer');
    }
}